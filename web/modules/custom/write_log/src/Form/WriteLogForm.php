<?php

namespace Drupal\write_log\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Validation\Plugin\Validation\Constraint\EmailConstraint;
use Symfony\Component\Validator\Constraints\EmailValidator;



class WriteLogForm extends FormBase {


  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['log'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Сообщение'),
      '#description' => $this->t('Сообщение в лог Drupal.'),
      '#required' => TRUE,
    ];


    if (\Drupal::currentUser()->isAnonymous()){
      $form['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Ваше имя'),
        '#description' => $this->t('Имя пользователя.'),
        '#required' => TRUE,
      ];

      $form['number_email'] = [
        '#type' => 'checkbox',
        '#title' => 'Анонимные пользователи могут указывать контактную информацию?',
      ];

      $form['accommodation'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'accommodation',
        ],

        '#states' => [
          'invisible' => [
            ':input[name="number_email"]' => ['checked' => FALSE],
          ],
        ],
      ];

      $form['accommodation']['phone'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Телефонный номер'),
        '#description' => $this->t('Пример телефонного номера +79261234567, 89261234567, 8(926)123-45-67'),
      ];

      $form['accommodation']['email'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Электронная почта'),
        '#description' => $this->t('Пример электронной почты pavel.gerasuta@mail.ru'),
      ];


    }



    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Отправить'),
    ];

    return $form;
  }


  public function getFormId() {
    return 'form_write_log';
  }


  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('phone')){

      $number = $form_state->getValue('phone');

      if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^',$number)) {
        $form_state->setErrorByName('phone', $this->t('Неправильный формат телефонного номера.'));
      }
    }

    if ($form_state->getValue('email')){

        $email = $form_state->getValue('email');
        $emailConstraint = new EmailConstraint();
        $emailConstraint->message = t('Неправильный формат электронной почты.');
        $emailValidator = new EmailValidator();
        try
        {
          $emailValidator->validate($email,$emailConstraint);        }
        catch(\Throwable $error)
        {
          $form_state->setErrorByName('email', $this->t('Неправильный формат электронной почты.'));
        }
    }


  }


  public function submitForm(array &$form, FormStateInterface $form_state) {

    $message = $form_state->getValue('log');
    $this->messenger()->addMessage($this->t('Спасибо за заполненную форму'));

    if (\Drupal::currentUser()->isAnonymous()){
      $message .= $form_state->getValue('name');
    }else{
      $message .= \Drupal::currentUser()->getAccountName();
    }



    \Drupal::logger('write_log')->notice($message);

  }

}
