<?php

namespace Drupal\form_task\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Page;


class FormTask extends FormBase {


  public function buildForm(array $form, FormStateInterface $form_state) {



    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Имя'),
      '#description' => $this->t('Ваше имя.'),
      '#required' => TRUE,
    ];


    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#description' => $this->t('Ваша электронная почта.'),
      '#required' => TRUE,
    ];


    $form['year_birth'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Год рождения'),
      '#options' => [
        '1' => $this->t('1981'),
        '2' => $this->t('1982'),
        '3' => $this->t('1983'),
        '4' => $this->t('1984'),
        '5' => $this->t('1985'),
        '6' => $this->t('1986'),
        '7' => $this->t('1987'),
        '8' => $this->t('1988'),
        '9' => $this->t('1989'),
        '10' => $this->t('1990'),
        '11' => $this->t('1991'),
        '12' => $this->t('1992'),
        '14' => $this->t('1993'),
        '15' => $this->t('1994'),
        '16' => $this->t('1995'),
        '17' => $this->t('1996'),
        '18' => $this->t('1997'),
        '19' => $this->t('1998'),
        '20' => $this->t('1999'),
        '21' => $this->t('2000'),
        '22' => $this->t('2001'),
        '23' => $this->t('2002'),
        '24' => $this->t('2003'),
        '25' => $this->t('2004'),
      ],
    ];


    $form['sex']= [
      '#type' => 'radios',
      '#title' => t('Ваш пол'),
      '#options' => [
        '0' => $this->t('Мужской'),
        '1' => $this->t('Женский'),
        '2' => $this->t('Другой'),
      ],
      '#required'=>TRUE,
      '#default_value' => 0,
    ];


    $form['limbs']= [
      '#type' => 'radios',
      '#title' => t('Количество конечностей'),
      '#options' => [
        '0' => $this->t('0'),
        '1' => $this->t('1'),
        '2' => $this->t('3'),
        '3' => $this->t('4'),
        '4' => $this->t('5'),
        '5' => $this->t('Больше'),
      ],
      '#required'=>TRUE,
      '#default_value' => 0,
    ];


    $form['superpower'] = [
      '#type' => 'select',
      '#title' => t('Суперсила'),
      '#options' => [
        '0' => $this->t('Бессмертие'),
        '1' => $this->t('Прохождение сквозь стены'),
        '2' => $this->t('Левитация'),
      ],
      '#multiple' => TRUE,
      '#required'=>TRUE,
    ];


    $form['biography'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Биография'),
      '#required' => TRUE,
    ];

    $form['check'] = [
      '#title' => t('C контрактом ознакомлен'),
      '#type' => 'checkbox',
      '#required'=>TRUE,
    ];




    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Отправить'),
    ];



    return $form;
  }


  public function getFormId() {
    return 'form_task';
  }


  public function submitForm(array &$form, FormStateInterface $form_state) {

    $super_value = array();
    foreach($form_state->getValue('superpower') as $value) {
      $super_value[] = $form['superpower']['#options'][$value];
    }

    $message = "Имя:". $form_state->getValue('name') . '<br>' .
    "Электронная почта:". $form_state->getValue('email') . '<br> ' .
    "Год рождения:". $form['year_birth']['#options'][$form_state->getValue('year_birth')] . '<br> ' .
    "Пол:".  $form['sex']['#options'][$form_state->getValue('sex')] . '<br> ' .
    "Количество конечностей:" . $form['limbs']['#options'][$form_state->getValue('limbs')] . '<br> ' .
    "Суперсила:" . implode(', ', $super_value).  '<br> ' .
    "Биография:". $form_state->getValue('biography');

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'form_task';
    $key = 'submit_form_task';
    $to = \Drupal::config('system.site')->get('mail');
    $params['message'] = $message;
    $params['mail_title'] = 'Form task';
    $langcode = 'ru';
    $send = true;

    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    if ($result['result'] !== true) {
      \Drupal::logger('form_task')->alert('There was a problem sending your message and it was not sent.');
      \Drupal::messenger()->addError(t('There was a problem sending your message and it was not sent.' ));
    }
    else {
      \Drupal::messenger()->addMessage(t('Your message has been sent. '
        . $params['mail_title']));
      \Drupal::logger('form_task')->notice('Your message has been sent. '
        . $params['mail_title']);
    }



  }

}
